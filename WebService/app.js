const express = require('express');
const MongoClient = require('mongodb').MongoClient;
const bodyParser = require('body-parser')

var dbName = "geoloc_db"
var url = "mongodb://geoloc_user:oklm123@localhost:27017/" + dbName

var app = express()

const updateDatabase = function(client, db, collection, query, changes) {
  return new Promise ( function(resolve, reject) {
    db.collection(collection).updateOne(query, changes, function(err, res){
      if (err) reject(new Error('Error : ', err))
      console.log("Success : ", res.result)
      console.log("Now closing the database client")
      client.close()
      resolve('Every thing went well')
    })
  })
}

const findInDatabase = function(client, db, collection, query, changes) {
  return new Promise ( function(resolve, reject) {
    db.collection(collection).find(query, {projection : changes}).toArray(function(err, res){
      if (err) reject(new Error('Error : ', err))
      console.log("Success : ", res.result)
      console.log("Now closing the database client")
      client.close()
      resolve(res)
    })
  })
}

function connectionToDatabase(action, collection, query, changes) {
  return new Promise( function(resolve, reject) {
    MongoClient.connect(url, function(err, client) {
      if (err) reject(new Error('Error', err))
      console.log("Connected successfully to server")
      let dbo = client.db(dbName)
      resolve(action(client, dbo, collection, query, changes))
    })
  })
}

app.use( bodyParser.json() );       // to support JSON-encoded bodies

app.get('/', function (req, res) {
  console.log('GET Geoloc API root')
  res.send('Hello there ! I am the Geoloc Web API\n')
})

/*
Emetteur : TTN
Paramètres :  logitude, latitude, date
Retour : OK
Envoi de la positon actuelle du boitier
*/
app.post('/newPosition', function (req, res) {
  console.log('POST newPositionn')
  console.log('date  :',  req.body.metadata.time)
  console.log('longitude :',req.body.metadata.gateways[0].longitude)
  console.log('latitude :', req.body.metadata.gateways[0].latitude)
  let query = { _id : "dupontjean"}
  let newPosition = {
    date: req.body.metadata.time,
    longitude: req.body.metadata.gateways[0].longitude,
    latitude: req.body.metadata.gateways[0].latitude
  }
  let changes = {
    $set: {
      lastPosition: newPosition
    },
    $push : {
      lifePosition: newPosition
    }
  }
  res.set('Content-Type', 'text/plain')
  connectionToDatabase(updateDatabase, "locations", query, changes)
  .then( (result) => {
    console.log('Finished successfully')
    res.send('New position added')
  })
  .catch( (err) => {console.log('Something wrong happened')
    res.send(err)
  })
})

/*
Emetteur : Front
Paramètres :
Retour : lastPosition (date, longitude, latitude)
Obtenir la dernière position
*/
app.get('/lastPosition', function(req, res) {
  console.log('GET lastPosition')
  console.log('req.query : ', req.query)
  let query = { _id : req.query.deviceID}
  let projection = {
     _id : 0,
     lastPosition : 1
  }
  res.set('Content-Type', 'application/json')
  connectionToDatabase(findInDatabase, "locations", query, projection)
  .then( (result) => {
    console.log('Finished successfully')
    console.log('Result : ', result)
    res.send(result[0].lastPosition)
  })
  .catch( (err) => {
    console.log('Something wrong happened')
    res.send(err)
  })
})

/*
Emetteur : Front
Paramètres :
Retour : lifePosition Array with date, longitude, latitude
Obtenir l'ensemble des positionss
*/
app.get('/lifePosition', function(req, res) {
  console.log('GET lifePosition')
  console.log('req.query : ', req.query)
  let query = { _id : "dupontjean"}
  let projection = {
     _id : 0,
     lifePosition : 1
  }
  res.set('Content-Type', 'application/json')
  connectionToDatabase(findInDatabase, "locations", query, projection)
  .then( (result) => {
    console.log('Finished successfully')
    res.send(result[0].lifePosition)
  })
  .catch( (err) => {
    console.log('Something wrong happened')
    res.send(err)
  })
})

app.listen(3000)
