#!/bin/sh
# This script create the database geoloc_db and the associated
# table and launch the mongod service
echo Beginning database creation
echo First let\'s delete the old Database
mongo --port 27017 < mongoScript
echo Database geoloc_db created
mongod --auth --port 27017 --dbpath /data/db1
