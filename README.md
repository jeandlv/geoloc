# Geoloc

Geolocalisation application based on the Lora technology and The Things Network.

This Readme focus only on how to install the WebServices (API, DataBase, Front End Server) and how to communicate between the Things Network and the installed API.

## Create the Web Server

Create an AWS ec2 instance.

### Configure the ec2 instance

Connect to the AWS instance with SSH

Install epel release

```sh
sudo yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
```

Install nginx

```sh
sudo yum install -y nginx
```

Install node

```sh
curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash -
sudo yum install -y nodejs
```

Install git

```sh
sudo yum install -y git
```

Install mongodb

Create a /etc/yum.repos.d/mongodb-org-4.0.repo file so that you can install MongoDB directly using yum:

```txt
[mongodb-org-4.0]
name=MongoDB Repository
baseurl=https://repo.mongodb.org/yum/redhat/$releasever/mongodb-org/4.0/x86_64/
gpgcheck=1
enabled=1
gpgkey=https://www.mongodb.org/static/pgp/server-4.0.asc
```

```sh
sudo yum install -y mongodb-org
```

### Get the sources

Create the working directory

```sh
mkdir /opt/geoloc
cd /opt/geoloc
```

Clone the repository in geoloc directory with git clone

Install the dependencies

```sh
cd WebService
npm install
```

### Configure the Web Server

Create a reverse proxy for the API and change the html root.

Update the /etc/nginx/nginx.conf configuration file with :

```txt
 server {
        listen       80 default_server;
        listen       [::]:80 default_server;
        server_name  _;
        root /opt/geoloc/Front;
        index index.php index.html index.htm;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        location / {
          try_files $uri $uri/ =404;
        }

        location /api/ {
          proxy_pass http://localhost:3000/;
        }

        error_page 404 /404.html;
            location = /40x.html {
        }

        error_page 500 502 503 504 /50x.html;
            location = /50x.html {
        }
    }
```

Initialise the Database

```sh
sudo bash /opt/geoloc/DataBasecreateDatabase.sh
```

Configure selinux and files permissions

```sh
sudo chown nginx.nginx /opt/geoloc/Front/index.html
sudo chmod u+rwx /opt/geoloc/Front/index.html
sudo setsebool -P httpd_can_network_connect on
sudo chcon -R -t httpd_sys_rw_content_t /opt/geoloc/Front/

sudo systemctl start nginx
```

Launch the API

```sh
sudo node /opt/geoloc/WebService/app.js
```

## Connect the application with the Things Network

Go to your application on the Things Network Website.

Go to Integration and click on "add integration". Select HTTP integration.

Select default-key in Access Key.

In the URL field enter : http://<aws-instance-ip-address>/api/newPosition/

Select the POST method.

In field "Custom Header Name" put "content-type".

In field "Custom Header Value" put "application/json".

Finally save.

When receiving datas the things network should now post them to geoloc API.

